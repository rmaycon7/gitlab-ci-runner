# # This image is used to create bleeding edge docker image and is not compatible with any other image
# FROM golang:1.6

# # Copy sources
# COPY . /go/src/gitlab.com/gitlab-org/gitlab-ci-multi-runner
# WORKDIR /go/src/gitlab.com/gitlab-org/gitlab-ci-multi-runner

# # Fetch tags (to have proper versioning)
# # RUN git fetch --tags || true

# # Build development version
# # ENV BUILD_PLATFORMS -osarch=linux/amd64
# RUN pwd
# RUN make 
# RUN	ln -s $(pwd)/out/binaries/gitlab-ci-multi-runner-linux-amd64 /usr/bin/gitlab-ci-multi-runner 
# RUN	ln -s $(pwd)/out/binaries/gitlab-ci-multi-runner-linux-amd64 /usr/bin/gitlab-runner

# # Install runner
# RUN packaging/root/usr/share/gitlab-runner/post-install

# # Preserve runner's data
# VOLUME ["/etc/gitlab-runner", "/home/gitlab-runner"]
# MAINTAINER Maycon Rebordão <mayconrebordao1122@gmail.com>

# USER mayconrebordao1122@gmail.com


# # init sets up the environment and launches gitlab-runner
# CMD ["run", "--user=gitlab-runner", "--working-directory=/home/gitlab-runner"]
# ENTRYPOINT ["/usr/bin/gitlab-runner"]


FROM ubuntu:14.04

ADD https://github.com/Yelp/dumb-init/releases/download/v1.0.2/dumb-init_1.0.2_amd64 /usr/bin/dumb-init
RUN chmod +x /usr/bin/dumb-init

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y ca-certificates wget apt-transport-https vim nano && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN echo "deb https://packages.gitlab.com/runner/gitlab-ci-multi-runner/ubuntu/ `lsb_release -cs` main" > /etc/apt/sources.list.d/runner_gitlab-ci-multi-runner.list && \
    wget -q -O - https://packages.gitlab.com/gpg.key | apt-key add - && \
    apt-get update -y && \
    apt-get install -y gitlab-ci-multi-runner && \
    wget -q https://github.com/docker/machine/releases/download/v0.7.0/docker-machine-Linux-x86_64 -O /usr/bin/docker-machine && \
    chmod +x /usr/bin/docker-machine && \
    apt-get clean && \
    mkdir -p /etc/gitlab-runner/certs && \
    chmod -R 700 /etc/gitlab-runner && \
    rm -rf /var/lib/apt/lists/*


# ADD entrypoint /
# RUN chmod +x /entrypoint

RUN mkdir /entrypoint
# ADD entrypoint /
RUN chmod +x /entrypoint

MAINTAINER Maycon Rebordão <mayconrebordao1122@gmail.com>

USER mayconrebordao1122@gmail.com


VOLUME ["/etc/gitlab-runner", "/home/gitlab-runner"]
ENTRYPOINT ["/usr/bin/dumb-init", "/entrypoint"]
CMD ["run", "--user=gitlab-runner", "--working-directory=/home/gitlab-runner"]